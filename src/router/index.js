import Vue from "vue";
import Router from "vue-router";
import store from "@/store";
import { global } from "@/api/user.js";

Vue.use(Router);

export const constantRoutes = [
  {
    // 注册页面
    path: "/register",
    component: () => import("@/views/Register")
  },
  // 微信权限认证页面
  {
    path: "/auth",
    component: () => import("@/views/Auth")
  },
  // 登录页面
  {
    path: "/login",
    component: () => import("@/views/Login")
  },
  // 多权限选择页面
  {
    path: "/choose-roles",
    component: () => import("@/views/ChooseRoles")
  },
  {
    path: "/",
    component: () => import("@/views/Homepage")
  }
];

export const asyncRoutes = [];

// 解决vue-router在3.0版本以上重复点菜单报错问题
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({
      y: 0
    }),
    routes: constantRoutes
  });

const router = createRouter();

export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
