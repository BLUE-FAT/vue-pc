import Vue from 'vue'
import '@/assets/js/utils/rem.js' //若需要适配需要
import ElementUI from 'element-ui';
import '@/assets/css/element-variables.scss';
import App from './App.vue'
import router from './router'
import store from './store'
import '@/assets/image/iconfont/iconfont.css'
import '@/assets/image/iconfont/iconfont.js'
import './router/permission'
import { global } from "@/js/globalConfig.js"
import permission from "@/assets/js/directive/permission"

Vue.prototype._global = global; //引入全局变量

Vue.use(ElementUI);
Vue.use(permission);//注册全局的鉴权指令 v-permission="['admin','teacher']"

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')